package com.hussein.stctrendingmovies.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.hussein.stctrendingmovies.domain.base.Unique
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProductionCompanies(
    val id: Int,
    @Json(name = "logo_path") val logoPath: String,
    val name: String,
    @Json(name = "origin_country") val originCountry: String
) : Unique {
    override val uniqueId: Int
        get() = id
}