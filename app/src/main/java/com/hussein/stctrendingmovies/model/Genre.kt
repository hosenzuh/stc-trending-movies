package com.hussein.stctrendingmovies.model

import com.hussein.stctrendingmovies.domain.base.Unique
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Genre(val id: Int, val name: String):Unique {
    override val uniqueId: Int
        get() = id
}
