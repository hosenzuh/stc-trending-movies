package com.hussein.stctrendingmovies.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.hussein.stctrendingmovies.domain.base.Unique
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProductionCountries(
    val name: String
) : Unique {
    override val uniqueId: Int
        get() = name.hashCode()
}