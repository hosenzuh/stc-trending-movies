package com.hussein.stctrendingmovies.domain.base

interface Unique {
    val uniqueId: Int
}