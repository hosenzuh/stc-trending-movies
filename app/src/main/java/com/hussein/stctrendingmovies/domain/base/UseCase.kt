package com.hussein.stctrendingmovies.domain.base

import com.hussein.stctrendingmovies.utils.ConnectionManager
import com.hussein.stctrendingmovies.utils.logcat
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

abstract class UseCase

abstract class ReceiveDataUseCase<T>(
    private val connectionManager: ConnectionManager,
) : UseCase() {

    protected fun execute(request: suspend () -> State<T>) = flow {
        if (!connectionManager.isConnected) {
            emit(State.Failure.InternetUnavailable())

            return@flow
        }

        emit(State.loading())
        emit(request())
    }.catch { throwable ->
        logcat(throwable)

        emit(State.failure())
    }
}