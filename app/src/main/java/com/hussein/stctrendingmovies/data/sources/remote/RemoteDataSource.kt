package com.hussein.stctrendingmovies.data.sources.remote

import com.hussein.stctrendingmovies.BuildConfig
import com.hussein.stctrendingmovies.data.sources.remote.interceptor.HeadersInterceptor
import com.hussein.stctrendingmovies.di.BaseUrl
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteDataSource @Inject constructor(
    @BaseUrl baseUrl: String,
    headersInterceptor: HeadersInterceptor,
    moshi: Moshi
) {

    val remoteService: RemoteService

    init {

        val okHttpClient = OkHttpClient.Builder().run {
            addInterceptor(headersInterceptor)

            if (BuildConfig.DEBUG) {
                val loggingInterceptor = HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }
                addInterceptor(loggingInterceptor)
            }

            build()
        }

        val retrofit = Retrofit.Builder().client(okHttpClient).baseUrl(baseUrl)
            .addConverterFactory(MoshiConverterFactory.create(moshi)).build()

        remoteService = retrofit.create()

    }
}