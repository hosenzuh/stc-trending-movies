package com.hussein.stctrendingmovies.data.sources.local

import android.content.Context
import androidx.room.Room
import com.hussein.stctrendingmovies.utils.APP_DATABASE_NAME
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class LocalDataSource @Inject constructor(
    @ApplicationContext context: Context,
) {

    val database: Database

    init {
        database = Room.databaseBuilder(
            context,
            Database::class.java,
            APP_DATABASE_NAME
        ).build()
    }
}