package com.hussein.stctrendingmovies.data.sources.remote.responses

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FailureBody(
    @Json(name = "status_message") val message: String
)
