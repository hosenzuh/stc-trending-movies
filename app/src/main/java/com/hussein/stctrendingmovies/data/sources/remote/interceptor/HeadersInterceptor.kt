package com.hussein.stctrendingmovies.data.sources.remote.interceptor

import com.hussein.stctrendingmovies.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class HeadersInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestWithHeaders = chain.request().newBuilder().run {
            addHeader("accept", "application/json")
            addHeader("Authorization", "Bearer ${BuildConfig.API_ACCESS_TOKEN}")

            build()
        }

        return chain.proceed(requestWithHeaders)
    }

}