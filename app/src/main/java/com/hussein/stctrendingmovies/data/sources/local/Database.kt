package com.hussein.stctrendingmovies.data.sources.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.hussein.stctrendingmovies.model.Genre
import com.hussein.stctrendingmovies.model.Movie
import com.hussein.stctrendingmovies.model.MovieDetails
import com.hussein.stctrendingmovies.utils.DatabaseVersions

//@Database(
//    entities = [Genre::class, Movie::class, MovieDetails::class],
//    version = DatabaseVersions.currentVersion
//)
abstract class Database : RoomDatabase() {

    abstract fun moviesDao(): MoviesDao
}