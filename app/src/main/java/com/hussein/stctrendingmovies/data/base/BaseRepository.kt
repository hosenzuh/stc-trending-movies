package com.hussein.stctrendingmovies.data.base

import com.hussein.stctrendingmovies.data.sources.remote.responses.FailureBody
import com.hussein.stctrendingmovies.domain.base.State
import com.hussein.stctrendingmovies.utils.logcat
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import retrofit2.Response
import java.util.concurrent.CancellationException

abstract class BaseRepository(protected val moshi: Moshi) {

    suspend fun <T> sendRemoteRequest(requestBlock: suspend () -> Response<T>): State<T> {

        var state: State<T>

        val response = requestBlock()
        state = try {
            if (response.isSuccessful) {

                val data = response.body()

                State.success(data)
            } else {

                val responseBody = response.errorBody()
                val errorBody = responseBody?.let {
                    val parameterizedType =
                        Types.newParameterizedType(FailureBody::class.java, Unit::class.java)
                    moshi.adapter<FailureBody>(parameterizedType).fromJson(it.string())
                }

                State.failure(errorBody?.message)
            }
        } catch (throwable: Throwable) {
            logcat(throwable)

            if (throwable is CancellationException) throw throwable

            State.failure()
        }

        return state
    }
}