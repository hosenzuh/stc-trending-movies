package com.hussein.stctrendingmovies.data.sources.remote

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.ToJson

class UnitJsonAdapter {

    @FromJson
    fun fromJson(jsonReader: JsonReader): Unit? {
        jsonReader.skipValue()

        return null
    }

    @ToJson
    fun toJson(jsonWriter: JsonWriter, @Suppress("UNUSED_PARAMETER") unit: Unit?) {
        jsonWriter.nullValue()
    }
}