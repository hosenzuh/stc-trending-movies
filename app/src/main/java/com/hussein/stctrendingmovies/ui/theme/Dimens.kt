package com.hussein.stctrendingmovies.ui.theme

import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

val Zero = 0.dp
val QuarterDefault = 4.dp
val HalfDefault = 8.dp
val ThreeQuarterDefault = 12.dp
val Default = 16.dp
val QuarterDoubleDefault = 20.dp
val HalfDoubleDefault = 24.dp
val ThreeQuarterDoubleDefault = 28.dp
val DoubleDefault = 32.dp

val QuarterDefaultFont = 4.sp
val HalfDefaultFont = 8.sp
val ThreeQuarterDefaultFont = 12.sp
val DefaultFont = 16.sp
val QuarterDoubleDefaultFont = 20.sp
val HalfDoubleDefaultFont = 24.sp
val ThreeQuarterDoubleDefaultFont = 28.sp
val DoubleDefaultFont = 32.sp