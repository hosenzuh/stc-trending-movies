package com.hussein.stctrendingmovies.utils

import android.util.Log

const val LOG_TAG = "STC Trending Movies"

fun logcat(message: String?) {
    Log.d(LOG_TAG, message ?: "null")
}

fun logcat(throwable: Throwable, message: String = "") {
    Log.e(LOG_TAG, message, throwable)
}