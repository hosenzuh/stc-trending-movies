package com.hussein.stctrendingmovies.utils

const val BASE_URL = "https://api.themoviedb.org/3/"

const val APP_DATABASE_VERSION = 1
const val APP_DATABASE_NAME = "ApplicationDatabase"

enum class DatabaseVersions(val versionNumber: Int) {
    FIRST_VERSION(1);

    companion object {
        const val currentVersion = 1
    }
}