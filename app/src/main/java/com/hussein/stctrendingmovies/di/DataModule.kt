package com.hussein.stctrendingmovies.di

import android.content.Context
import androidx.room.Room
import com.hussein.stctrendingmovies.data.sources.local.Database
import com.hussein.stctrendingmovies.data.sources.local.LocalDataSource
import com.hussein.stctrendingmovies.data.sources.remote.RemoteDataSource
import com.hussein.stctrendingmovies.data.sources.remote.UnitJsonAdapter
import com.hussein.stctrendingmovies.utils.APP_DATABASE_NAME
import com.hussein.stctrendingmovies.utils.BASE_URL
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Qualifier

@Qualifier
@Retention(value = AnnotationRetention.BINARY)
annotation class BaseUrl

@Module()
@InstallIn(SingletonComponent::class)
object DataModule {

    @Provides
    @BaseUrl
    fun provideBaseUrl() = BASE_URL

    @Provides
    fun provideMoshi(): Moshi = Moshi.Builder().add(UnitJsonAdapter()).build()

    @Provides
    fun provideRemoteService(remoteDataSource: RemoteDataSource) =
        remoteDataSource.remoteService

    @Provides
    fun provideDatabase(localDataSource: LocalDataSource) = localDataSource.database

}